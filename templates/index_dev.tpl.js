console.log('start index_dev.js');

d3.csv('../df_data.csv', function(d) {
    return {
        year: +d.year,
        month: +d.month,
        value: +d.value,
    };
}).then(function(data) {
    const years = data.map(e => e.year);
    const minYear = Math.min(...years);
    const maxYear = Math.max(...years);
    const nbYear = maxYear - minYear + 1;

    const values = data.map(e => e.value);
    const minValue = Math.min(...values);
    const maxValue = Math.max(...values);

    const months = [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec',
    ];
    const angles = d3.range(0, 360, 360 / months.length).map(e => e - 360 / 4);
    angleMonths = d3.zip(angles, months);

    const floorValue = Math.floor(minValue);
    const capValue = Math.floor(maxValue) + 1;
    const minDomain = floorValue;
    const maxDomain = capValue + 0.5;
    const shift = minDomain;

    const refCircles = [
        { value: 0 - shift, text: '+0°C' },
        { value: 1.5 - shift, text: '+1.5°C' },
        { value: 2 - shift, text: '+2.0°C' },
    ];

    console.log(`minValue = ${minValue}, maxValue = ${maxValue}`);
    console.log(`floorValue = ${floorValue}, capValue = ${capValue}`);
    console.log(`minDomain = ${minDomain}, maxDomain = ${maxDomain}`);
    console.log(`shift = ${shift}`);

    const dataFull = data.map((d, i) => {
        return {
            angle: (i * 2 * Math.PI) / 12,
            radius: d.value - shift,
            year: d.year,
            month: d.month,
        };
    });
    console.log(dataFull[0]);
    console.log(dataFull[1]);
    window.dataFull = dataFull;

    const width = 900,
        height = 600,
        radius = Math.min(width, height) / 2 - 30;

    const r = d3
        .scaleLinear()
        .domain([minDomain - shift, maxDomain - shift])
        .range([0, radius]);

    const line = d3
        .radialLine()
        .angle(function(d) {
            return d.angle;
        })
        .radius(function(d) {
            return r(d.radius);
        });

    const svg = d3
        .select('#anchor-__$data.uuid$__')
        .append('svg')
        .attr('width', width)
        .attr('height', height)
        .append('g')
        .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')');

    const gr = svg
        .append('g')
        .attr('class', 'r axis')
        .selectAll('g')
        .data(refCircles)
        .enter()
        .append('g');

    gr.append('circle').attr('r', function(d) {
        return r(d.value);
    });

    gr.append('text')
        .attr('y', function(d) {
            return -r(d.value) - 4;
        })
        .attr('transform', 'rotate(0)')
        .style('text-anchor', 'middle')
        .text(function(d) {
            return d.text;
        });

    const ga = svg
        .append('g')
        .attr('class', 'a axis')
        .selectAll('g')
        .data(angleMonths)
        .enter()
        .append('g')
        .attr('transform', function(d) {
            return 'rotate(' + d[0] + ')';
        });

    ga.append('line').attr('x2', radius);

    ga.append('text')
        .attr('x', radius + 6)
        .attr('dx', '.0em')
        .attr('dy', '.0em')
        .attr('transform', function(d) {
            return d[0] < 180 && d[0] > 0
                ? 'rotate(-90 ' + (radius + 6) + ',0)'
                : 'rotate(90 ' + (radius + 6) + ',0)';
        })
        .style('text-anchor', 'middle')
        .style('font-size', 15)
        .text(function(d) {
            return d[1];
        });

    const arrPath = d3.range(nbYear).map(() => svg.append('path'));

    const dot = svg.append('circle');

    const date = svg
        .append('text')
        .attr('dy', '.35em')
        .style('text-anchor', 'middle')
        .style('font-size', '16px');

    const chunkData = function(data) {
        const dataLocal = data.slice(0);
        const chunkInit = dataLocal.splice(0, 12);
        const chunks = [chunkInit];
        while (dataLocal.length) {
            let chunk = dataLocal.splice(0, 12);
            lastChunk = chunks[chunks.length - 1];
            lastElt = lastChunk[lastChunk.length - 1];
            chunk.unshift(lastElt);
            chunks.push(chunk);
        }
        return chunks;
    };

    const chunkDataPad = function(data, nbArray) {
        let chunks = chunkData(data);
        for (let i in d3.range(nbArray - chunks.length)) chunks.push([]);
        return chunks;
    };

    const color = d3.interpolateRainbow;
    window.color = color;

    const updatePlot = function(n) {
        const data = dataFull.slice(0, +n + 1);
        const lineEnd = data[data.length - 1];
        const point = d3.pointRadial(lineEnd.angle, r(lineEnd.radius));
        const chunks = chunkDataPad(data, nbYear);

        // debug
        window.data = data;
        window.lineEnd = lineEnd;
        window.point = point;
        window.chunks = chunks;

        for ([i, path] of arrPath.entries()) {
            path.datum(chunks[i])
                .attr('class', 'line')
                .attr('stroke', () => {
                    return d3.rgb(color(i / nbYear));
                })
                .attr('d', line);
        }

        dot.attr('class', 'dot')
            .attr('cx', point[0])
            .attr('cy', point[1])
            .attr('r', 5)
            .attr('fill', 'black');
        date.text(lineEnd.year);
    };

    const showValue = function(n) {
        d = dataFull[n];
        return months[d.month - 1] + ' ' + d.year;
    };

    const manipulate = makeManipulate({
        name: 'Date :',
        min: 0,
        max: data.length - 1,
        step: 1,
        init: 0,
        callback: updatePlot,
        showValue: showValue,
        sliderValueWidth: 70,
        // nameColor: 'red',
    });
    document.getElementById('anchor-__$data.uuid$__').appendChild(manipulate);

    updatePlot(0);
});
