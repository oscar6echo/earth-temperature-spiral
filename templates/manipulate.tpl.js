function makeManipulate({
    name = 'MyName',
    min = 1,
    max = 100,
    step = 1,
    init = 10,
    callback = n => console.log(n),
    showValue = n => n,
    nameWidth = 50,
    nameColor = null,
    sliderWidth = 300,
    sliderValueWidth = 50,
    sliderValueTextAlign = 'center',
    intervalInit = 100,
    intervalCoef = 1.25,
    playing = true,
} = {}) {
    // random uuid
    const randomUuid = () => Math.floor(Math.random() * 1e6);
    const uuid = randomUuid();

    // html name
    const divName = document.createElement('div');
    divName.className = 'slider-name';
    divName.innerHTML = name;
    divName.style.width = `${nameWidth}px`;
    if (nameColor) {
        divName.style.color = nameColor;
    }

    // button play pause
    const buttonPlayPause = document.createElement('button');
    buttonPlayPause.className = 'btn button-play-pause';
    buttonPlayPause.state = 'Play';

    const imgButtonPlay = document.createElement('i');
    imgButtonPlay.className = 'fa fa-play';

    const imgButtonPause = document.createElement('i');
    imgButtonPause.className = 'fa fa-pause';

    buttonPlayPause.appendChild(imgButtonPlay);
    buttonPlayPause.appendChild(imgButtonPause);

    buttonPlayPause.children[1].style.display = 'none';

    // button stop
    const buttonStop = document.createElement('button');
    buttonStop.className = 'btn button-stop';

    const imgButtonStop = document.createElement('i');
    imgButtonStop.className = 'fa fa-stop';
    buttonStop.appendChild(imgButtonStop);

    // button faster
    const buttonFaster = document.createElement('button');
    buttonFaster.className = 'btn button-faster';
    buttonFaster.title = 'Faster';

    const imgButtonFaster = document.createElement('i');
    imgButtonFaster.className = 'fa fa-chevron-up';
    buttonFaster.appendChild(imgButtonFaster);

    // button slower
    const buttonSlower = document.createElement('button');
    buttonSlower.className = 'btn button-slower';
    buttonSlower.title = 'Slower';

    const imgButtonSlower = document.createElement('i');
    imgButtonSlower.className = 'fa fa-chevron-down';
    buttonSlower.appendChild(imgButtonSlower);

    // button direction
    const buttonDirection = document.createElement('button');
    buttonDirection.className = 'btn button-reverse';
    buttonDirection.state = 'Right';
    buttonDirection.title = 'Play Direction';

    const imgButtonDirectionRight = document.createElement('i');
    imgButtonDirectionRight.className = 'fa fa-long-arrow-right';

    const imgButtonDirectionLeft = document.createElement('i');
    imgButtonDirectionLeft.className = 'fa fa-long-arrow-left';

    buttonDirection.appendChild(imgButtonDirectionRight);
    buttonDirection.appendChild(imgButtonDirectionLeft);

    buttonDirection.children[1].style.display = 'none';

    // button loop
    const buttonLoop = document.createElement('button');
    buttonLoop.className = 'btn button-loop';
    buttonLoop.state = false;
    buttonLoop.title = 'Play in Loop';

    const imgButtonLoop = document.createElement('i');
    imgButtonLoop.className = 'fa fa-retweet';
    buttonLoop.appendChild(imgButtonLoop);

    // slider
    const slider = document.createElement('input');

    slider.className = 'slider';
    slider.setAttribute('type', 'range');
    slider.setAttribute('min', min);
    slider.setAttribute('max', max);
    slider.setAttribute('step', step);
    slider.setAttribute('value', init);
    slider.style.width = `${sliderWidth}px`;

    // slider value
    const sliderValue = document.createElement('div');
    sliderValue.className = 'slider-value';
    sliderValue.innerHTML = showValue(init);
    sliderValue.style.width = `${sliderValueWidth}px`;
    sliderValue.style.textAlign = sliderValueTextAlign;

    // manipulate
    const manipulate = document.createElement('div');
    manipulate.className = 'manipulate';
    manipulate.id = 'manipulate-' + uuid;

    manipulate.appendChild(divName);
    manipulate.appendChild(buttonPlayPause);
    manipulate.appendChild(buttonStop);
    manipulate.appendChild(buttonFaster);
    manipulate.appendChild(buttonSlower);
    manipulate.appendChild(buttonDirection);
    manipulate.appendChild(buttonLoop);
    manipulate.appendChild(slider);
    manipulate.appendChild(sliderValue);

    // ACTIONS

    // slider.onchange = function () {
    slider.oninput = function() {
        const n = this.value;
        sliderValue.innerHTML = showValue(n);
        callback(n);
    };

    let interval = intervalInit;
    let timer = null;

    const takeStep = function() {
        let n = +slider.value;
        const sign = buttonDirection.state == 'Right' ? +1 : -1;
        let update = false;
        if (sign > 0) {
            if (n < max) {
                n += 1;
                update = true;
            } else {
                if (buttonLoop.state) {
                    n = min;
                    update = true;
                }
            }
        } else {
            if (n > min) {
                n -= 1;
                update = true;
            } else {
                if (buttonLoop.state) {
                    n = max;
                    update = true;
                }
            }
        }
        if (update) {
            callback(n);
            slider.value = n;
            sliderValue.innerHTML = showValue(n);
        }
    };

    const setVariableInterval = function() {
        takeStep();
        timer = setTimeout(setVariableInterval, interval);
    };

    // button play pause
    buttonPlayPause.onclick = function() {
        if (buttonPlayPause.state == 'Play') {
            console.log('button Play clicked');
            buttonPlayPause.children[0].style.display = 'none';
            buttonPlayPause.children[1].style.display = '';
            buttonPlayPause.state = 'Pause';

            setVariableInterval();
        } else {
            console.log('button Pause clicked');
            buttonPlayPause.children[0].style.display = '';
            buttonPlayPause.children[1].style.display = 'none';
            buttonPlayPause.state = 'Play';

            clearInterval(timer);
        }
    };

    // button stop
    buttonStop.onclick = function() {
        buttonPlayPause.children[0].style.display = '';
        buttonPlayPause.children[1].style.display = 'none';
        buttonPlayPause.state = 'Play';
        clearInterval(timer);
        const n = min;
        callback(n);
        slider.value = n;
    };

    // button direction
    buttonDirection.onclick = function() {
        console.log('button Direction clicked');
        if (buttonDirection.state == 'Right') {
            buttonDirection.children[0].style.display = 'none';
            buttonDirection.children[1].style.display = '';
            buttonDirection.state = 'Left';
        } else {
            buttonDirection.children[0].style.display = '';
            buttonDirection.children[1].style.display = 'none';
            buttonDirection.state = 'Right';
        }
    };

    // button loop
    buttonLoop.onclick = function() {
        console.log('button Loop clicked');
        if (buttonLoop.state == false) {
            buttonLoop.style.backgroundColor = '#cccccc';
            buttonLoop.state = true;
        } else {
            buttonLoop.style.backgroundColor = '';
            buttonLoop.state = false;
        }
    };

    // button faster
    buttonFaster.onclick = function() {
        console.log('button Faster clicked');
        interval /= intervalCoef;
        console.log(`interval = ${interval}`);
    };

    // button slower
    buttonSlower.onclick = function() {
        console.log('button Slower clicked');
        interval *= intervalCoef;
        console.log(`interval = ${interval}`);
    };

    if (playing) {
        buttonPlayPause.click();
    }
    return manipulate;
}
