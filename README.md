# Earth Temperature Spiraling towards +2°C

On 9 May 2016 [Ed Hawkins](http://www.climate-lab-book.ac.uk/author/ed/), a climate scientist at the University of Reading in the United Kingdom [published](http://www.climatecentral.org/news/see-earths-temperature-spiral-toward-2c-20332) this striking animated graph featuring a rainbow-colored record of global temperatures spinning outward from the late 19th century to the present as the Earth heats up.  

<img src="http://assets.climatecentral.org/images/uploads/news/5_9_16_Andrea_TempSpiralEdHawkins.gif" width="500px">


Some periods stand out:
+ 1877-78: strong El Nino event warms global temperatures
+ 1880s-1910: small cooling, partially due to volcanic eruptions
+ 1910-1940s: warming, partially due to recovery from volcanic eruptions, small increase in solar output and natural variability
+ 1950s-1970s: fairly flat temperatures as cooling sulphate aerosols mask the greenhouse gas warming
+ 1980-now: strong warming, with temperatures pushed higher in 1998 and 2016 due to strong El Nino events

This repo contains a [notebook](http://nbviewer.jupyter.org/urls/gitlab.com/oscar6echo/earth-temperature-spiral/raw/master/climate-temperature-spiral.ipynb) which enables you to: 
+ update the data each time you run it
+ allow better navigation across time

To go straight to the result see [this page](https://glcdn.githack.com/oscar6echo/earth-temperature-spiral/raw/a0a44e69b55037fdb5a50bcc6de63fe65a62c604/data/index.html).

